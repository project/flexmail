# Flexmail

## Documentation
This module has implemented not all, but most of the endpoints documented in `https://api.flexmail.eu/documentation/#overview`.

## Multiple accounts
This module supports multiple Flexmail accounts.
Using the FlexmailAccount config entities you can keep track of multiple Flexmail accounts.

## Webform support
Enable the `flexmail_webform` module to have access to the FlexmailWebformHandler.
A FlexmailwebformHandlerBase has been provided that can be used to extend the already provided functionality.

### Custom fields in flexmail
There is support for custom fields in flexmail.
Using the api we retrieve what custom fields are present and then provide the corresponding input fields.

Currently the following custom field types are support:
* Date ["date", "hidden"]
* Free text ["textfield", "number", "email", "tel", "url", "hidden"]
* Multiple choice ["checkboxes"]
* Numeric ["number", "hidden"]

Additional custom field types can be defined using the annotation `@FlexCustomField`.

