<?php

namespace Drupal\flexmail\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class FlexmailAccountForm.
 *
 * Form to create or update an FlexmailAccount entity.
 */
class FlexmailAccountForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\flexmail\Entity\FlexmailAccount
     */
    $flexmail_account = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $flexmail_account->label(),
      '#description' => $this->t("Label for the Flexmail account."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $flexmail_account->id(),
      '#machine_name' => [
        'exists' => '\Drupal\flexmail\Entity\FlexmailAccount::load',
      ],
      '#disabled' => !$flexmail_account->isNew(),
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#maxlength' => 255,
      '#default_value' => $flexmail_account->getUrl(),
      '#description' => $this->t('Flexmail endpoint URL'),
      '#required' => TRUE,
    ];

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#maxlength' => 255,
      '#default_value' => $flexmail_account->getAccountId(),
      '#description' => $this->t('Flexmail account ID.'),
      '#required' => TRUE,
    ];

    $form['personal_access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Personal access token'),
      '#maxlength' => 255,
      '#default_value' => $flexmail_account->getPersonalAccessToken(),
      '#description' => $this->t('Flexmail account access token.'),
      '#required' => TRUE,
    ];

    $form['help_text'] = [
      '#markup' => $this->t('More information on how to receive Account ID and Personal access token is available here: @link', [
        '@link' => Link::fromTextAndUrl('Flexmail API', Url::fromUri('https://api.flexmail.eu/documentation/#auth', ['absolute' => TRUE]))->toString(),
      ]
      ),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $flexmail_account = $this->entity;
    $status = $flexmail_account->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Flexmail account.', [
            '%label' => $flexmail_account->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Flexmail account.', [
            '%label' => $flexmail_account->label(),
          ]));
    }
    $form_state->setRedirectUrl($flexmail_account->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    $element['test_connection'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test connection'),
      '#submit' => ['::submitForm', '::testConnection'],
    ];
    return $element;
  }

  /**
   * Form submission handler for the 'testConnection' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function testConnection(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\flexmail\Entity\FlexmailAccount $flexmail_account */
    $flexmail_account = $this->entity;

    /** @var \Drupal\flexmail\api\service\FlexmailApiFactory $factory */
    $factory = \Drupal::service('flexmail.factory');
    $manager = $factory->get($flexmail_account);

    if ($manager->getFlexmailClient()->testConnection()) {
      $this->messenger()->addMessage($this->t('Connection succeeded!'));
    }
    else {
      $this->messenger()->addError($this->t('Connection failed! Check logs for details.'));
    }

    $form_state->setRebuild();
  }

}
