<?php

namespace Drupal\flexmail\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FlexmailSettingsForm.
 *
 * Configure some of the essential settings for the flexmail integration.
 */
class FlexmailSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'flexmail.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flexmail_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('flexmail.settings');

    $form['pagination_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Pagination limit'),
      '#description' => $this->t('When Flexmail API returns paginated results, this number indicates how many results are requested per page.'),
      '#default_value' => $config->get('pagination_limit') ?? 250,
      '#min' => 5,
      '#max' => 500,
      '#step' => 5,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('flexmail.settings')
      ->set('pagination_limit', $form_state->getValue('pagination_limit'))
      ->save();
  }

}
