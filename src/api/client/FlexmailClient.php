<?php

namespace Drupal\flexmail\api\client;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\flexmail\api\exception\FlexmailAPIException;
use Drupal\flexmail\Entity\FlexmailAccount;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

/**
 * Flexmail client.
 */
class FlexmailClient implements FlexmailClientInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Guzzle Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Flexmail API url (absolute).
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * Authentication token (account_id:personal_access_token in base64).
   *
   * @var string
   */
  protected $authentication;

  /**
   * Constructs an FlexmailClient object.
   *
   * @param \Drupal\flexmail\Entity\FlexmailAccount $flex_mail_account
   *   Flexmail account.
   */
  public function __construct(FlexmailAccount $flex_mail_account) {
    $this->client = new Client();
    $this->baseUrl = trim($flex_mail_account->getUrl(), " \t\n\r/");
    $this->authentication = base64_encode($flex_mail_account->getAccountId() . ':' . $flex_mail_account->getPersonalAccessToken());
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection(): bool {
    try {
      $this->sendRequest('GET', '/');
      return TRUE;
    }
    catch (\Exception $e) {
      $this->getLogger('Flexmail')
        ->error("Something went wrong while testing the flexmail connection. {$e->getMessage()} {$e->getTraceAsString()}");
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendRequest(string $method, string $uri, array $params = [], string $body = ''): array {
    $headers = [
      'Authorization' => 'Basic ' . $this->authentication,
    ];

    // Prefix with slash if it's not there yet.
    $uri = strpos($uri, '/') === 0 ? $uri : ('/' . $uri);
    $url = $this->baseUrl . $uri;

    // Add timeout option.
    $params['timeout'] = 30;

    try {
      $request = new Request($method, $url, $headers, $body);
      $response = $this->client->send($request, $params);
      return [
        'body' => Json::decode($response->getBody()),
        'headers' => $response->getHeaders(),
        'code' => $response->getStatusCode(),
      ];
    }
    catch (RequestException $e) {
      // Log errors.
      $response = $e->getResponse();

      if (!empty($response)) {
        $message = $response->getBody()->getContents();
      }

      // Fallback message in case body is empty.
      if (empty($message)) {
        $message = $e->getMessage();
      }

      $exception = new FlexmailAPIException($message, $e->getCode(), $e);
      $this->getLogger('Flexmail')->error(
        $this->t('Flexmail API exception - @message.', ['@message' => $e->getMessage()])
      );

      throw $exception;
    }
  }

}
