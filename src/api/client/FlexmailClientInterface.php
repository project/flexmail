<?php

namespace Drupal\flexmail\api\client;

/**
 * Interface FlexmailClientInterface.
 *
 * @package Drupal\flexmail
 */
interface FlexmailClientInterface {

  /**
   * Send request.
   *
   * @param string $method
   *   Request method.
   * @param string $uri
   *   Request URI.
   * @param array $params
   *   Additional connection params.
   * @param string $body
   *   Request body.
   *
   * @return array
   *   Response.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function sendRequest(string $method, string $uri, array $params = [], string $body = ''): array;

  /**
   * Test the connection.
   *
   * @return bool
   *   Returns true if connection succeeded.
   */
  public function testConnection(): bool;

}
