<?php

namespace Drupal\flexmail\api\service;

use Drupal\flexmail\api\value\Contact;
use Drupal\flexmail\api\value\InterestCollection;

/**
 * Interface FlexmailApiServiceInterface.
 *
 * @package Drupal\flexmail
 */
interface FlexmailApiManagerInterface {

  /**
   * Get contacts from Flexmail account.
   *
   * @return array
   *   An array with contacts from the Flexmail account.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getContacts(): array;

  /**
   * Get the contact from Flexmail account with the given email.
   *
   * @param string $email
   *   The email of the contact you want to fetch.
   *
   * @return array
   *   The contact that matches the email.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getContactWithEmail(string $email): array;

  /**
   * Get custom fields from Flexmail account.
   *
   * @return array
   *   An array with all the custom fields from the flexmail account.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getCustomFields(): array;

  /**
   * Gets preferences.
   *
   * @return array
   *   An array with the preferences of the flexmail account.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @deprecated Interest labels are deprecated and superseded by Interests.
   */
  public function getPreferences(): array;

  /**
   * Gets preferences for contact.
   *
   * @param int $contactId
   *   The ID of the contact.
   *
   * @return array
   *   An array with preferences for the given contact ID.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @deprecated Interest labels are deprecated and superseded by Interests.
   */
  public function getContactPreferenceSubscriptions(int $contactId): array;

  /**
   * Gets all interests of a Flexmail account.
   *
   * @param array $query
   *   Add additional query parameters. See Flexmail API to see the options.
   *
   * @return \Drupal\flexmail\api\value\InterestCollection
   *   A collection of Interest objects.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getInterests(array $query = []): InterestCollection;

  /**
   * Gets interests of contact.
   *
   * @param int $contactId
   *   The id of the contact.
   *
   * @return \Drupal\flexmail\api\value\InterestCollection
   *   A collection of Interest objects.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getContactInterests(int $contactId): InterestCollection;

  /**
   * Gets all possible interest labels.
   *
   * @return array
   *   An array with all the possible interest labels of the Flexmail account.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @deprecated Interest labels are deprecated and superseded by Interests.
   */
  public function getInterestLabels(): array;

  /**
   * Gets interest labels for contact.
   *
   * @param int $contactId
   *   The id of the contact.
   *
   * @return array
   *   An array with all the possible interest labels for the contact.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @deprecated Interest labels are deprecated and superseded by Interests.
   */
  public function getContactInterestLabels(int $contactId): array;

  /**
   * Get all possible sources for a contact.
   *
   * @return array
   *   An array with all the possible sources for a contact.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getSources(): array;

  /**
   * Creates new contact.
   *
   * @param \Drupal\flexmail\api\value\Contact $contact
   *   A Contact object.
   *
   * @return int|null
   *   The result from the flexmail API.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function createContact(Contact $contact): ?int;

  /**
   * Updates existing contact.
   *
   * @param int $id
   *   The id of the existing contact.
   * @param \Drupal\flexmail\api\value\Contact $contact
   *   A contact object with the new values you want to set for the contact.
   *
   * @return bool
   *   Whether or not the update has succeeded.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @throws \InvalidArgumentException
   */
  public function updateContact(int $id, Contact $contact): bool;

  /**
   * Subscribes contact to preference.
   *
   * @param int $contactId
   *   The id of the existing contact.
   * @param int $preferenceId
   *   The id of the preference.
   *
   * @return bool
   *   Whether or not the subscription has succeeded.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @deprecated Interest labels are deprecated and superseded by Interests.
   */
  public function subscribeContactToPreference(int $contactId, int $preferenceId): bool;

  /**
   * Subscribes contact to interest label.
   *
   * @param int $contactId
   *   The id of the contact.
   * @param int $preferenceId
   *   The id of the preference.
   *
   * @return bool
   *   Whether or not the subscription has succeeded.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   * @deprecated Interest labels are deprecated and superseded by Interests.
   */
  public function subscribeContactToInterestLabel(int $contactId, int $preferenceId): bool;

  /**
   * Subscribes contact to interest.
   *
   * @param int $contactId
   *   The id of the contact.
   * @param string $interestId
   *   The id of the interest.
   *
   * @return bool
   *   Whether the subscription has succeeded.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function subscribeContactToInterest(int $contactId, string $interestId): bool;

}
