<?php

namespace Drupal\flexmail\api\service;

use Drupal\flexmail\Entity\FlexmailAccount;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class FlexmailApiFactory.
 *
 * @package Drupal\flexmail
 */
class FlexmailApiFactory implements FlexmailApiFactoryInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * An array of FlexmailAPI Managers.
   *
   * @var array
   */
  protected $managers = [];

  /**
   * {@inheritdoc}
   */
  public function get(FlexmailAccount $flexmail_account) {
    if (!isset($this->managers[$flexmail_account->id()])) {
      /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
      $config_factory = $this->container->get('config.factory');
      $this->managers[$flexmail_account->id()] = new FlexmailApiManager($flexmail_account, $config_factory);
    }
    return $this->managers[$flexmail_account->id()];
  }

}
