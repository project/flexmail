<?php

namespace Drupal\flexmail\api\service;

use Drupal\flexmail\Entity\FlexmailAccount;

/**
 * Interface FlexmailApiFactoryInterface.
 *
 *  Defines the method with which we will retrieve the Flexmail api manager for a certain Flexmail account.
 *
 * @package Drupal\flexmail
 */
interface FlexmailApiFactoryInterface {

  /**
   * Retrieves FlexmailApiManager for requester flexmail_account.
   *
   * @param \Drupal\flexmail\Entity\FlexmailAccount $flexmail_account
   *   The service api name.
   *
   * @return \Drupal\flexmail\api\service\FlexmailApiManagerInterface
   *   The registered FlexmailApiManager for this Flexmail Account.
   */
  public function get(FlexmailAccount $flexmail_account);

}
