<?php

namespace Drupal\flexmail\api\service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\flexmail\api\client\FlexmailClient;
use Drupal\flexmail\api\value\Contact;
use Drupal\flexmail\api\value\InterestCollection;
use Drupal\flexmail\Entity\FlexmailAccount;

/**
 * Flexmail API service.
 */
class FlexmailApiManager implements FlexmailApiManagerInterface {

  /**
   * Flexmail client.
   *
   * @var \Drupal\flexmail\api\client\FlexmailClient
   */
  protected $flexmailClient;

  /**
   * Flexmail settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $flexmailSettings;

  /**
   * Constructs an FlexmailAPIService object.
   *
   * @param \Drupal\flexmail\Entity\FlexmailAccount $flexmail_account
   *   Flexmail Account.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(FlexmailAccount $flexmail_account, ConfigFactoryInterface $config_factory) {
    $this->flexmailClient = new FlexmailClient($flexmail_account);
    $this->flexmailSettings = $config_factory->get('flexmail.settings');
  }

  /**
   * Gets Flexmail Client.
   *
   * @return \Drupal\flexmail\api\client\FlexmailClient
   *   The Flexmail Client.
   */
  public function getFlexmailClient() {
    return $this->flexmailClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getContacts(): array {
    return $this->getResourceList('/contacts');
  }

  /**
   * {@inheritdoc}
   */
  public function getContactWithEmail(string $email): array {
    // TODO encoding isn't necessary?
    // $encodedEmail = urlencode($email);
    return $this->getResourceList('/contacts', ['email' => $email]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomFields(): array {
    $results = $this->getResourceList('/custom-fields');
    return $results['_embedded']['item'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPreferences(): array {
    return $this->getResourceList('/preferences');
  }

  /**
   * {@inheritdoc}
   */
  public function getContactPreferenceSubscriptions($contactId): array {
    return $this->getResourceList('/contacts/' . $contactId . '/preferences');
  }

  /**
   * {@inheritdoc}
   */
  public function getInterests(array $query = []): InterestCollection {
    $response = $this->getResourceList('/interests', $query);

    if (!empty($response['total']) && isset($response['_embedded']['item'])) {
      return InterestCollection::fromArray($response['_embedded']['item']);
    }

    return InterestCollection::fromArray([]);
  }

  /**
   * {@inheritdoc}
   */
  public function getContactInterests(int $contactId): InterestCollection {
    $response = $this->getResourceList('/contacts/' . $contactId . '/interest-subscriptions');

    if (!empty($response['total']) && isset($response['_embedded']['item'])) {
      $normalizedResponse = [];
      foreach ($response['_embedded']['item'] as $item) {
        $normalizedResponse[] = $item['_embedded']['interest'];
      }

      return InterestCollection::fromArray($normalizedResponse);
    }

    return InterestCollection::fromArray([]);
  }

  /**
   * {@inheritdoc}
   */
  public function getInterestLabels(): array {
    return $this->getResourceList('/interest-labels');
  }

  /**
   * {@inheritdoc}
   */
  public function getContactInterestLabels($contactId): array {
    return $this->getResourceList('/contacts/' . $contactId . '/interest-labels');
  }

  /**
   * {@inheritdoc}
   */
  public function getSources(): array {
    return $this->getResourceList('/sources');
  }

  /**
   * Helper function which gets all resources (paginated) from given URI.
   *
   * @param string $uri
   *   The URI to address from the flexmail API.
   * @param array $extraParameters
   *   Extra parameters that need to be passed to the API.
   *
   * @return array
   *   The result form the request.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  protected function getResourceList(string $uri, array $extraParameters = []): array {
    $offset = 0;
    $limit = $this->flexmailSettings->get('pagination_limit') ?? 250;

    $resources = [];
    do {
      $parameters['query'] = [
        'limit' => $limit,
        'offset' => $offset,
      ];

      // The union of the query param arrays.
      // Note that $parameters['query'] values will not be replaced if the same key exists in $extraParameters.
      $parameters['query'] = $parameters['query'] + $extraParameters;

      $result = $this->flexmailClient->sendRequest('GET', $uri, $parameters);
      $resources = array_merge($resources, $result['body']);

      $offset += $limit;

      // Handle non-paginated requests.
      if (empty($result['headers']['totalitems'][0])) {
        break;
      }

    } while ($result['headers']['totalitems'][0] >= $offset);

    return $resources;
  }

  /**
   * {@inheritdoc}
   */
  public function createContact(Contact $contact): ?int {
    $result = $this->flexmailClient->sendRequest('POST', '/contacts', [], Json::encode($contact));
    if (empty($result['headers']['location'][0])) {
      return NULL;
    }
    else {
      $location_parts = explode('/', $result['headers']['location'][0]);
      return end($location_parts);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateContact(int $id, Contact $new_contact_data): bool {
    $result = $this->flexmailClient->sendRequest('GET', '/contacts/' . $id);
    // First check if we received existing contact data.
    if (empty($result['body']) || $result['code'] === 404) {
      return FALSE;
    }
    else {
      $existing_contact = Contact::createFromAssocArray($result['body']);
      $existing_contact->update($new_contact_data);
      $result = $this->flexmailClient->sendRequest('PUT', '/contacts/' . $id, [], Json::encode($existing_contact));
      // If update is successful, body should be empty.
      return empty($result['body']) && $result['code'] === 204;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function subscribeContactToPreference(int $contactId, int $preferenceId): bool {
    $result = $this->flexmailClient->sendRequest('POST', '/contact-preference-subscriptions', [], Json::encode([
      'contact_id' => $contactId,
      'preference_id' => $preferenceId,
    ]));
    return !empty($result['headers']['location']);
  }

  /**
   * {@inheritdoc}
   */
  public function subscribeContactToInterestLabel(int $contactId, int $interestLabelId): bool {
    $result = $this->flexmailClient->sendRequest('POST', '/contact-interest-label-subscriptions', [], Json::encode([
      'contact_id' => $contactId,
      'interest_label_id' => $interestLabelId,
    ]));
    return !empty($result['headers']['location']);
  }

  /**
   * {@inheritdoc}
   */
  public function subscribeContactToInterest(int $contactId, string $interestId): bool {
    $result = $this->flexmailClient->sendRequest('POST', '/contacts/' . $contactId . '/interest-subscriptions', [], Json::encode([
      'interest_id' => $interestId,
    ]));

    return !empty($result['headers']['location']);
  }
}
