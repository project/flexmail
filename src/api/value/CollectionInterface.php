<?php

namespace Drupal\flexmail\api\value;

use IteratorAggregate;

/**
 * Interface for Collection objects.
 *
 * All collection objects must implement at least this interface.
 *
 * @package Drupal\flexmail
 */
interface CollectionInterface extends IteratorAggregate, ValueInterface {

}
