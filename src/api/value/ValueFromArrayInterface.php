<?php

namespace Drupal\flexmail\api\value;

/**
 * Interface adding the possibility to create the value object from array data.
 *
 * The fromArray method returns "self", hence implementing classes are supposed
 * to be defined "final".
 *
 * @package Drupal\flexmail
 */
interface ValueFromArrayInterface {

  /**
   * Returns a value object based on values extracted from the given array.
   *
   * @param array $data
   *   The data to create the value from.
   *
   * @return static
   */
  public static function fromArray(array $data): self;

}
