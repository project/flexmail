<?php

namespace Drupal\flexmail\api\value;


/**
 * Resembles a Flexmail Interest.
 *
 * @package Drupal\flexmail
 */
final class Interest extends ValueBase implements ValueFromArrayInterface {

  /**
   * Public interest visibility.
   */
  const INTEREST_PUBLIC = 'public';

  /**
   * Private interest visibility.
   */
  const INTEREST_PRIVATE = 'private';

  /**
   * ID of the object.
   *
   * @var string
   */
  private string $id;

  /**
   * Name of the object.
   *
   * @var string
   */
  private string $name;

  /**
   * Visibility of the interest.
   *
   * @var string
   */
  private string $visibility;

  /**
   * Label of the object.
   *
   * @var string
   */
  private string $label;

  /**
   * Description of the object.
   *
   * @var string
   */
  private string $description;

  /**
   * Relative link url.
   *
   * @var string
   */
  private string $link;

  /**
   * Use only the named constructors.
   */
  private function __construct() {
    // The constructor is private:
    // Create the object using the named constructors.
  }

  /**
   * Return default array values for instantiating an Interest object.
   *
   * @return array
   */
  protected static function defaultValues() {
    return [
      'label' => '',
      'description' => '',
      '_links' => ['self' => ['href' => '']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $data): self {
    $data += self::defaultValues();

    self::validateFieldsNotEmpty(['id', 'name', 'visibility'], $data);

    $interest = new self();
    $interest->id = $data['id'];
    $interest->name = $data['name'];
    $interest->visibility = $data['visibility'];
    $interest->label = $data['label'];
    $interest->description = $data['description'];
    $interest->link = $data['_links']['self']['href'];

    return $interest;
  }

  /**
   * Get the id from the object.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Get the name of the interest.
   *
   * @return string
   *   The name of the interest.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Get the visibility of the interest.
   *
   * @return string
   *   The visibility of the interest ("private" or "public").
   */
  public function getVisibility(): string {
    return $this->visibility;
  }

  /**
   * Get the label of the interest.
   *
   * @return string
   *   The label of the interest.
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * Get the description of the interest.
   *
   * @return string
   *   The description of the interest.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Get the link associated with the interest.
   *
   * @return string
   *   The link associated with the interest.
   */
  public function getLink(): string {
    return $this->link;
  }

  /**
   * Check if interest is public.
   *
   * @return bool
   *   True if interest is public.
   */
  public function isPublic(): bool {
    return $this->visibility === self::INTEREST_PUBLIC;
  }

  /**
   * Check if interest is private.
   *
   * @return bool
   *   True if interest is public.
   */
  public function isPrivate(): bool {
    return $this->visibility === self::INTEREST_PRIVATE;
  }

  /**
   * {@inheritdoc}
   */
  public function sameValueAs(ValueInterface $object): bool {
    /** @var \Drupal\flexmail\api\value\Interest $object */
    return
      $this->sameValueTypeAs($object)
      && $this->getId() === $object->getId()
      && $this->getName() === $object->getName()
      && $this->getVisibility() === $object->getVisibility()
      && $this->getLabel() === $object->getLabel()
      && $this->getDescription() === $object->getDescription()
      && $this->getLink() === $object->getLink();
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->getLabel() ?: $this->getName();
  }
}
