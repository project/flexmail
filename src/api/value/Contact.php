<?php

namespace Drupal\flexmail\api\value;

/**
 * Resembles a Flexmail Contact.
 *
 * @package Drupal\flexmail
 */
final class Contact extends ValueBase implements \JsonSerializable {

  /**
   * ID of the object.
   *
   * @var int|null
   */
  private $id;

  /**
   * The email of the contact.
   *
   * @var string
   */
  private $email;

  /**
   * The first name of the contact.
   *
   * @var string
   */
  private $firstName;

  /**
   * The name of the contact.
   *
   * @var string
   */
  private $name;

  /**
   * The language of the contact.
   *
   * @var string
   */
  private $language;

  /**
   * The custom fields of the contact.
   *
   * @var array
   */
  private $customFields;

  /**
   * The source of the contact.
   *
   * @var int
   */
  private $source;

  /**
   * Contact constructor.
   *
   * @param string $email
   *   Contact email.
   * @param string $language
   *   Contact language.
   * @param int|null $source
   *   Contact source.
   * @param null $id
   *   Contact internal ID.
   * @param string $firstName
   *   Contact first name.
   * @param string $name
   *   Contact last name.
   * @param array $customFields
   *   Contact custom fields..
   */
  public function __construct(string $email, string $language, ?int $source, $id = NULL, string $firstName = '', string $name = '', array $customFields = []) {
    $this->email = $email;
    $this->language = $language;
    $this->source = $source;
    $this->id = $id;
    $this->firstName = $firstName;
    $this->name = $name;
    $this->customFields = $customFields;
  }

  /**
   * Creates Contact from array of values.
   *
   * @param array $values
   *   An associative array from which a contact should be created.
   *
   * @return \Drupal\flexmail\api\value\Contact
   *   A Contact object created from the array of values.
   *
   * @throws \InvalidArgumentException
   */
  public static function createFromAssocArray(array $values) {
    $email = isset($values['email']) ? (string) $values['email'] : '';
    $language = isset($values['language']) ? (string) $values['language'] : '';

    if (empty($email) || empty($language)) {
      throw new \InvalidArgumentException('Mandatory properties are missing to create Contact. (email, language)');
    }

    $source = isset($values['source']) ? (integer) $values['source'] : NULL;
    $id = isset($values['id']) ? (integer) $values['id'] : NULL;
    $firstName = isset($values['first_name']) ? (string) $values['first_name'] : '';
    $name = isset($values['name']) ? (string) $values['name'] : '';
    $customFields = isset($values['custom_fields']) ? (array) $values['custom_fields'] : [];

    return new Contact($email, $language, $source, $id, $firstName, $name, $customFields);
  }

  /**
   * Get the id from the object.
   *
   * @return int|null
   *   The id of the object or NULL if not set.
   */
  public function getId(): ?int {
    return $this->id;
  }

  /**
   * Set the object ID.
   *
   * @param int $id
   *   A numeric ID.
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * Get the email from the contact.
   *
   * @return string
   *   The email of the contact.
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * Set the email for the contact.
   *
   * @param string $email
   *   An email address.
   */
  public function setEmail(string $email): void {
    $this->email = $email;
  }

  /**
   * Get the first name of the contact.
   *
   * @return string
   *   The first name of the contact.
   */
  public function getFirstName(): string {
    return $this->firstName;
  }

  /**
   * Set the first name for the contact.
   *
   * @param string $firstName
   *   A first name.
   */
  public function setFirstName(string $firstName): void {
    $this->firstName = $firstName;
  }

  /**
   * Get the name of the contact.
   *
   * @return string
   *   The name of the contact.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Set the name for the contact.
   *
   * @param string $name
   *   A name.
   */
  public function setName(string $name): void {
    $this->name = $name;
  }

  /**
   * Get the language of the contact.
   *
   * @return string
   *   The language of the contact.
   */
  public function getLanguage(): string {
    return $this->language;
  }

  /**
   * Set the language for the contact.
   *
   * @param string $language
   *   The new language for the contact.
   */
  public function setLanguage(string $language): void {
    $this->language = $language;
  }

  /**
   * Get the custom fields for this contact.
   *
   * @return array
   *   The custom fields for this contact.
   */
  public function getCustomFields(): array {
    return $this->customFields;
  }

  /**
   * Set the custom fields for this contact.
   *
   * @param array $customFields
   *   An array with custom fields.
   */
  public function setCustomFields(array $customFields): void {
    $this->customFields = $customFields;
  }

  /**
   * Get the source id.
   *
   * @return int
   *   The id of the source.
   */
  public function getSource(): int {
    return $this->source;
  }

  /**
   * Set the source.
   *
   * @param int $source
   *   The id of the source.
   */
  public function setSource(int $source): void {
    $this->source = $source;
  }

  /**
   * Serialize the object data do a JSON.
   */
  public function jsonSerialize(): array {
    $contact = [];

    if (!empty($this->email)) {
      $contact['email'] = (string) $this->email;
    }

    if (!empty($this->language)) {
      $contact['language'] = (string) $this->language;
    }

    if (!empty($this->source)) {
      $contact['source'] = (integer) $this->source;
    }

    if (!empty($this->firstName)) {
      $contact['first_name'] = (string) $this->firstName;
    }

    if (!empty($this->name)) {
      $contact['name'] = (string) $this->name;
    }

    if (!empty($this->customFields)) {
      $contact['custom_fields'] = (array) $this->customFields;
    }

    return $contact;
  }

  /**
   * Updates this contact with new Contact data.
   *
   * @param \Drupal\flexmail\api\value\Contact $new_contact_data
   *   The new data of the new contact.
   */
  public function update(Contact $new_contact_data): void {
    $this->email = $new_contact_data->email ?? $this->email;
    $this->language = $new_contact_data->language ?? $this->language;
    $this->source = $new_contact_data->source ?? $this->source;
    $this->id = $new_contact_data->id ?? $this->id;
    $this->firstName = $new_contact_data->firstName ?? $this->firstName;
    $this->name = $new_contact_data->name ?? $this->name;
    $this->customFields = $new_contact_data->customFields ?? $this->customFields;
  }

  /**
   * {@inheritdoc}
   */
  public function sameValueAs(ValueInterface $object): bool {
    /** @var \Drupal\flexmail\api\value\Contact $object */
    return
      $this->sameValueTypeAs($object)
      && $this->getId() === $object->getId()
      && $this->getName() === $object->getName()
      && $this->getFirstName() === $object->getFirstName()
      && $this->getEmail() === $object->getEmail()
      && $this->getSource() === $object->getSource()
      && $this->getLanguage() === $object->getLanguage()
      && $this->getCustomFields() === $object->getCustomFields();
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    $string = $this->getEmail();
    $name = trim($this->getFirstName() . ' ' . $this->getFirstName());

    if ($name) {
      return $string . ' (' . $name . ')';
    }

    return $string;
  }
}
