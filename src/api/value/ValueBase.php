<?php

namespace Drupal\flexmail\api\value;

use InvalidArgumentException;

/**
 * Abstract value class.
 *
 * @package Drupal\flexmail
 */
abstract class ValueBase implements ValueInterface {

  /**
   * {@inheritdoc}
   */
  public function sameValueTypeAs(ValueInterface $object): bool {
    return $this::class === $object::class;
  }

  /**
   * Validates that the provided fields are not empty.
   *
   * @param array $fields
   *   An array of field names to check for.
   * @param array $data
   *   The data array to check for missing fields.
   *
   * @throws InvalidArgumentException
   *   Thrown if any of the specified fields are missing.
   */
  protected static function validateFieldsNotEmpty(array $fields, array $data): void {
    foreach ($fields as $field) {
      if (!isset($data[$field]) || $data[$field] === '') {
        throw new InvalidArgumentException("The required $field field is empty.");
      }
    }
  }

}
