<?php

namespace Drupal\flexmail\api\value;

use IteratorAggregate;
use ArrayIterator;

/**
 * Resembles a Flexmail Interest Collection.
 *
 * @package Drupal\flexmail
 */
final class InterestCollection extends CollectionBase implements ValueFromArrayInterface, IteratorAggregate {

  /**
   * Use only the named constructors.
   */
  private function __construct() {
    // The constructor is private:
    // Create the object using the named constructors.
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $data): self {
    $collection = new self();

    foreach ($data as $interestData) {
      $interest = Interest::fromArray($interestData);
      $collection->values[$interest->getId()] = $interest;
    }

    return $collection;
  }
}
