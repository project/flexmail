<?php

namespace Drupal\flexmail\api\value;

use ArrayIterator;

/**
 * Abstract collection class.
 *
 * @package Drupal\flexmail
 */
abstract class CollectionBase extends ValueBase implements CollectionInterface {

  /**
   * Collection of values.
   *
   * @var \Drupal\flexmail\api\value\ValueInterface[]
   */
  protected array $values = [];

  /**
   * Compare two Collection objects and check if they can be considered equal.
   *
   * @param \Drupal\flexmail\api\value\ValueInterface $object
   *   Value object to compare this collection with.
   *
   * @return bool
   *   Both have the same values.
   */
  public function sameValueAs(ValueInterface $object): bool {
    if (!$this->sameValueTypeAs($object)) {
      return FALSE;
    }

    /** @var \Drupal\flexmail\api\value\CollectionInterface $object */
    if (!$this->sameCollectionCount($object)) {
      return FALSE;
    }

    if (!$this->sameCollectionValues($object)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get the ArrayIterator containing all values in this collection.
   *
   * @return \ArrayIterator
   *   The array iterator of the object values.
   */
  public function getIterator(): ArrayIterator {
    return new ArrayIterator($this->values);
  }

  /**
   * Check if the collection has the same amount of items as this collection.
   *
   * @param \Drupal\flexmail\api\value\CollectionInterface $collection
   *   Collection to compare this collection with.
   *
   * @return bool
   *   Has the same amount of items.
   */
  protected function sameCollectionCount(CollectionInterface $collection): bool {
    return $this->getIterator()->count() === $collection->getIterator()->count();
  }

  /**
   * Check if the collection has the same values as this collection.
   *
   * @param \Drupal\flexmail\api\value\CollectionInterface $collection
   *   Collection to compare this collection with.
   *
   * @return bool
   *   Has the same values.
   */
  protected function sameCollectionValues(CollectionInterface $collection): bool {
    /** @var \ArrayIterator $collectionIterator */
    $collectionIterator = $collection->getIterator();
    foreach ($this as $index => $item) {
      if (!$collectionIterator->offsetExists($index)) {
        return FALSE;
      }

      $collectionItem = $collectionIterator->offsetGet($index);
      if (!$item->sameValueAs($collectionItem)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    $labels = [];
    foreach ($this->values as $value) {
      $labels[] = (string) $value;
    }

    return implode(', ', $labels);
  }

}
