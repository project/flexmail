<?php

namespace Drupal\flexmail\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Flexmail account entities.
 */
interface FlexmailAccountInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
