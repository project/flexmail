<?php

namespace Drupal\flexmail\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Flexmail account entity.
 *
 * @ConfigEntityType(
 *   id = "flexmail_account",
 *   label = @Translation("Flexmail account"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\flexmail\FlexmailAccountListBuilder",
 *     "form" = {
 *       "add" = "Drupal\flexmail\Form\FlexmailAccountForm",
 *       "edit" = "Drupal\flexmail\Form\FlexmailAccountForm",
 *       "delete" = "Drupal\flexmail\Form\FlexmailAccountDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\flexmail\FlexmailAccountHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "flexmail_account",
 *   admin_permission = "administer flexmail accounts",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "url",
 *     "account_id",
 *     "personal_access_token"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/services/flexmail/flexmail_account/{flexmail_account}",
 *     "add-form" = "/admin/config/services/flexmail/flexmail_account/add",
 *     "edit-form" = "/admin/config/services/flexmail/flexmail_account/{flexmail_account}/edit",
 *     "delete-form" = "/admin/config/services/flexmail/flexmail_account/{flexmail_account}/delete",
 *     "collection" = "/admin/config/services/flexmail/flexmail_account"
 *   }
 * )
 */
class FlexmailAccount extends ConfigEntityBase implements FlexmailAccountInterface {

  /**
   * The Flexmail account ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Flexmail account label.
   *
   * @var string
   */
  protected $label;

  /**
   * The flexmail url.
   *
   * @var string
   */
  protected $url;

  /**
   * The account id.
   *
   * @var string
   */
  protected $account_id;


  /**
   * The personal access token.
   *
   * @var string
   */
  protected $personal_access_token;

  /**
   * Returns the personal access token.
   *
   * @return string
   *   personal access token.
   */
  public function getPersonalAccessToken(): string {
    if (!empty($this->personal_access_token)) {
      return $this->personal_access_token;
    }
    else {
      return '';
    }
  }

  /**
   * Returns the account id.
   *
   * @return string
   *   account id.
   */
  public function getAccountId(): string {
    if (!empty($this->account_id)) {
      return $this->account_id;
    }
    else {
      return '';
    }
  }

  /**
   * Returns the flexmail url.
   *
   * @return string
   *   url
   */
  public function getUrl(): string {
    if (!empty($this->url)) {
      return $this->url;
    }
    else {
      // Provide default endpoint.
      return 'https://api.flexmail.eu/';
    }
  }

}
