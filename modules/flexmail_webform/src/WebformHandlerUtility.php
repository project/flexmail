<?php

namespace Drupal\flexmail_webform;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\flexmail\api\service\FlexmailApiFactoryInterface;
use Drupal\flexmail\api\service\FlexmailApiManagerInterface;
use Drupal\webform\Plugin\WebformHandlerInterface;

/**
 * Class WebformHandlerUtility.
 *
 * @package Drupal\flexmail_webform.
 */
class WebformHandlerUtility {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flexmail factory.
   *
   * @var \Drupal\flexmail\api\service\FlexmailApiFactoryInterface
   */
  protected $flexmailFactory;

  /**
   * FlexCustomValues constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\flexmail\api\service\FlexmailApiFactoryInterface $flexmail_factory
   *   Flex mail factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    FlexmailApiFactoryInterface $flexmail_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->flexmailFactory = $flexmail_factory;
  }

  /**
   * Get flexmail manager.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $webform_handler
   *   Webform handler;.
   *
   * @return \Drupal\flexmail\api\service\FlexmailApiManagerInterface|null
   *   Flexmail Api Manager.
   */
  public function getFlexmailManager(WebformHandlerInterface $webform_handler): ?FlexmailApiManagerInterface {
    $configuration = $webform_handler->getConfiguration();
    $flexmail_account = $configuration['settings']['account'] ?? NULL;

    try {
      /** @var \Drupal\flexmail\Entity\FlexmailAccount $flexmail_account */
      $flexmail_account = $this->entityTypeManager->getStorage('flexmail_account')
        ->load($flexmail_account);
    }
    catch (\Exception $e) {
      return NULL;
    }

    return $flexmail_account ? $this->flexmailFactory->get($flexmail_account) : NULL;
  }

}
