<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\webform\WebformInterface;

/**
 * Interface WebformElementMatcherInterface.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
interface WebformElementMatcherInterface {

  /**
   * Get options for flex type.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   Webform entity.
   * @param string $flex_type
   *   Flex field type.
   *
   * @return array
   *   Select options.
   */
  public function getOptionsForFlexType(WebformInterface $webform, string $flex_type): array;

}
