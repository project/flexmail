<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\Component\Plugin\PluginBase;

/**
 * Class FlexCustomFieldBase.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
abstract class FlexCustomFieldBase extends PluginBase implements FlexCustomFieldInterface {

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritDoc}
   */
  public function getSupportedWebformElementTypes(): array {
    return $this->pluginDefinition['supported_webform_element_types'];
  }

  /**
   * Check if flex allows multiple values.
   *
   * @return bool
   *   True is field allow multiple values.
   */
  protected function allowsMultipleValues(): bool {
    $allows_multiple_values = $this->configuration['allows_multiple_values'] ?? FALSE;

    return (bool) $allows_multiple_values;
  }

}
