<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Interface FlexCustomValuesInterface.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
interface FlexCustomValuesInterface {

  /**
   * Get Values from submission.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $webform_handler
   *   Webform handler.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return array
   *   Flex custom values.
   */
  public function getValuesFromSubmission(
    WebformHandlerInterface $webform_handler,
    WebformSubmissionInterface $webform_submission
  ): array;

}
