<?php

namespace Drupal\flexmail_webform\CustomFields;

/**
 * Interface FlexCustomFieldInterface.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
interface FlexCustomFieldInterface {

  /**
   * Provide a description of the plugin.
   *
   * @return string
   *   A string description.
   */
  public function description();

  /**
   * Supported webform element types.
   *
   * @return string[]
   */
  public function getSupportedWebformElementTypes(): array;

  /**
   * Format value typed by user.
   *
   * @param $element_value
   *   Typed value by the end user.
   *
   * @return mixed
   *   Flexmail value.
   */
  public function formatTypedValue($element_value);

  /**
   * Format static value.
   *
   * @param $element_value
   *   Value stored in handler configuration.
   *
   * @return mixed
   *   Flexmail value.
   */
  public function formatStaticValue($element_value);

  /**
   * Form element where static value will typed by administrator.
   *
   * @return array
   *   Webform element.
   */
  public function getElementForStaticValue(): array;

}
