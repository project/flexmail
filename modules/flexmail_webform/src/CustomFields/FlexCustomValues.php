<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\flexmail\api\exception\FlexmailAPIException;
use Drupal\flexmail_webform\WebformHandlerUtility;
use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class FlexCustomValues.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
class FlexCustomValues implements FlexCustomValuesInterface {

  /**
   * Custom field builder.
   *
   * @var \Drupal\flexmail_webform\CustomFields\CustomFieldBuilderInterface
   */
  protected $customFieldBuilder;

  /**
   * Handler utility.
   *
   * @var \Drupal\flexmail_webform\WebformHandlerUtility
   *   Handler utility.
   */
  protected $handlerUtility;

  /**
   * Flex field manager.
   *
   * @var \Drupal\flexmail_webform\CustomFields\FlexCustomFieldPluginManager
   */
  protected $flexFieldManager;

  /**
   * Flex field data received from endpoint.
   *
   * @var array
   */
  protected $flexFieldData = [];

  /**
   * FlexCustomValues constructor.
   *
   * @param \Drupal\flexmail_webform\CustomFields\CustomFieldBuilderInterface $custom_field_builder
   *   Custom field builder.
   * @param \Drupal\flexmail_webform\WebformHandlerUtility $handler_utility
   *   Handler utility.
   * @param \Drupal\flexmail_webform\CustomFields\FlexCustomFieldPluginManager $flex_field_manager
   *   Flex field manager.
   */
  public function __construct(
    CustomFieldBuilderInterface $custom_field_builder,
    WebformHandlerUtility $handler_utility,
    FlexCustomFieldPluginManager $flex_field_manager
  ) {
    $this->customFieldBuilder = $custom_field_builder;
    $this->handlerUtility = $handler_utility;
    $this->flexFieldManager = $flex_field_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function getValuesFromSubmission(
    WebformHandlerInterface $webform_handler,
    WebformSubmissionInterface $webform_submission
  ): array {
    $custom_flex_values = [];
    $this->initiateFlexFieldData($webform_handler);

    $configuration_mappings = $this->getActiveConfigurationForCustomFields($webform_handler);
    foreach ($configuration_mappings as $flex_field_key => $configuration_mapping) {
      try {
        $flex_plugin = $this->getFlexFieldPlugin($flex_field_key);
        if ($this->isOtherOptionSelected($configuration_mapping)) {
          $flex_field_value = $this->getStaticValueStoredInConfiguration($flex_plugin, $configuration_mapping);
        }
        else {
          $flex_field_value = $this->getTypedValueByUser($flex_plugin, $configuration_mapping, $webform_submission);
        }
      }
      catch (PluginException $e) {
        $flex_field_value = NULL;
      }

      $custom_flex_values[$flex_field_key] = $flex_field_value;
    }

    return $custom_flex_values;
  }

  /**
   * Initiate flex field data.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $webform_handler
   *   Webform handler.
   */
  protected function initiateFlexFieldData(WebformHandlerInterface $webform_handler) {
    $flexmail_manager = $this->handlerUtility->getFlexmailManager($webform_handler);
    if (!$flexmail_manager) {
      return;
    }

    try {
      foreach ($flexmail_manager->getCustomFields() as $flex_field) {
        $this->flexFieldData[$flex_field['placeholder']] = $flex_field;
      }
    }
    catch (FlexmailAPIException $e) {
    }
  }

  /**
   * Get active configuration for custom flex fields.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $webform_handler
   *   Webform handler.
   *
   * @return array
   *   Mapping configuration.
   */
  protected function getActiveConfigurationForCustomFields(WebformHandlerInterface $webform_handler) {
    return array_filter(
      $this->customFieldBuilder->getCustomSettings($webform_handler),
      function ($settings) {
        return !empty($settings[CustomFieldBuilder::SETTING_MAPPER_NAME]);
      });
  }

  /**
   * Get flex field plugin.
   *
   * @param $flex_field_key
   *   Flex field key.
   *
   * @return \Drupal\flexmail_webform\CustomFields\FlexCustomFieldInterface|null
   *   Flex field plugin
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getFlexFieldPlugin($flex_field_key): ?FlexCustomFieldInterface {
    $flex_field_data = $this->flexFieldData[$flex_field_key];

    /** @var \Drupal\flexmail_webform\CustomFields\FlexCustomFieldInterface $flex_plugin */
    $flex_plugin = $this->flexFieldManager->createInstance(
      $flex_field_data['type'],
      $flex_field_data
    );

    return $flex_plugin;
  }

  /**
   * Is other option selected.
   *
   * @param array $configuration_mapping
   *   Configuration mapping.
   *
   * @return bool
   *   True if other option has been selected
   */
  protected function isOtherOptionSelected(array $configuration_mapping): bool {
    return $configuration_mapping[CustomFieldBuilder::SETTING_MAPPER_NAME] === CustomFieldBuilder::OTHER_OPTION;
  }

  /**
   * Get typed value by the user.
   *
   * @param \Drupal\flexmail_webform\CustomFields\FlexCustomFieldInterface $flex_plugin
   *   Flex plugin.
   * @param array $configuration_mapping
   *   Configuration mapping.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return mixed
   *   Flex value
   */
  protected function getTypedValueByUser(
    FlexCustomFieldInterface $flex_plugin,
    array $configuration_mapping,
    WebformSubmissionInterface $webform_submission
  ) {
    $submission_data = $webform_submission->getData();
    $element_key = $configuration_mapping[CustomFieldBuilder::SETTING_MAPPER_NAME];
    $element_value = $submission_data[$element_key] ?? NULL;

    return $flex_plugin->formatTypedValue($element_value);
  }

  /**
   * Get static value stored in configuration.
   *
   * @param \Drupal\flexmail_webform\CustomFields\FlexCustomFieldInterface $flex_plugin
   *   Flex plugin.
   * @param array $configuration_mapping
   *   Configuration mapping.
   *
   * @return mixed
   *   Flex value
   */
  protected function getStaticValueStoredInConfiguration(
    FlexCustomFieldInterface $flex_plugin,
    array $configuration_mapping
  ) {
    $element_value = $configuration_mapping[CustomFieldBuilder::SETTING_OTHER_VALUE] ?? NULL;

    return $flex_plugin->formatStaticValue($element_value);
  }

}
