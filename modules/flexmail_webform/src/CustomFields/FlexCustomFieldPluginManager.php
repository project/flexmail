<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\flexmail_webform\Annotation\FlexCustomField;

/**
 * Class FlexCustomFieldPluginManager.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
class FlexCustomFieldPluginManager extends DefaultPluginManager {

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    $sub_dir = 'Plugin/Flex/CustomField';
    $plugin_interface = FlexCustomFieldInterface::class;
    $plugin_definition_annotation_name = FlexCustomField::class;

    parent::__construct($sub_dir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);

    $this->alterInfo('flex_custom_field_info');
    $this->setCacheBackend($cache_backend, 'flex_custom_field_info', ['flex_custom_field_info']);
  }

}
