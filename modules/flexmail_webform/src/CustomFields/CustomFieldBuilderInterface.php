<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\flexmail\api\service\FlexmailApiManagerInterface;
use Drupal\webform\Plugin\WebformHandlerInterface;

/**
 * Interface CustomFieldBuilderInterface.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
interface CustomFieldBuilderInterface {

  /**
   * Build form elements to map custom flexmail fields.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $handler
   *   Webform Handler.
   * @param \Drupal\flexmail\api\service\FlexmailApiManagerInterface|null $flexmailApiManager
   *   Flexmail manager.
   *
   * @return array
   *   Form elements.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function buildCustomMappingFields(WebformHandlerInterface $handler, ?FlexmailApiManagerInterface $flexmailApiManager): array;

  /**
   * Get custom flex filed settings for handler.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $handler
   *   Webform handler.
   *
   * @return array
   *   Settings.
   */
  public function getCustomSettings(WebformHandlerInterface $handler): array;

}
