<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\webform\WebformInterface;

/**
 * Class WebformElementMatcher.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
class WebformElementMatcher implements WebformElementMatcherInterface {

  /**
   * Flex field manager.
   *
   * @var \Drupal\flexmail_webform\CustomFields\FlexCustomFieldPluginManager
   */
  protected $flexFieldManager;

  /**
   * WebformElementMatcher constructor.
   *
   * @param \Drupal\flexmail_webform\CustomFields\FlexCustomFieldPluginManager $flex_field_manager
   *   Flex field manager.
   */
  public function __construct(FlexCustomFieldPluginManager $flex_field_manager) {
    $this->flexFieldManager = $flex_field_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function getOptionsForFlexType(WebformInterface $webform, string $flex_type): array {
    $options = [];

    $supported_webform_types = $this->getSupportedWebformTypes($flex_type);
    $webform_elements = $this->filterWebformElements($webform, $supported_webform_types);
    foreach ($webform_elements as $element) {
      $options[$element['#webform_key']] = $element['#title'];
    }

    return $options;
  }

  /**
   * Get supported webform types.
   *
   * @param string $flex_type
   *   Flex field type.
   *
   * @return string[]
   *   Webform element types.
   */
  protected function getSupportedWebformTypes(string $flex_type): array {
    try {
      /** @var \Drupal\flexmail_webform\CustomFields\FlexCustomFieldInterface $custom_flex_field */
      $custom_flex_field = $this->flexFieldManager->createInstance($flex_type);
      return $custom_flex_field->getSupportedWebformElementTypes();
    }
    catch (PluginException $e) {
      return [];
    }
  }

  /**
   * Filter webform elements to specific types.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   Webform entity.
   * @param array $webformTypes
   *   Webform types.
   *
   * @return array
   *   Webform elements.
   */
  protected function filterWebformElements(WebformInterface $webform, array $webformTypes): array {
    $elements = $webform->getElementsInitializedAndFlattened();

    return array_filter($elements, function ($element) use ($webformTypes) {
      return in_array($element['#type'], $webformTypes);
    });
  }

}
