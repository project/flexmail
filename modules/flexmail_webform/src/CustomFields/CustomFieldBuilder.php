<?php

namespace Drupal\flexmail_webform\CustomFields;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\flexmail\api\service\FlexmailApiManagerInterface;
use Drupal\flexmail_webform\StringTranslation\FlexWebformStringTranslationTrait;
use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformInterface;

/**
 * Class CustomFieldBuilder.
 *
 * @package Drupal\flexmail_webform\CustomFields
 */
class CustomFieldBuilder implements CustomFieldBuilderInterface {

  use FlexWebformStringTranslationTrait;

  /**
   * Other option value.
   */
  const OTHER_OPTION = '_other_';

  /**
   * Name of setting where mapping value is stored.
   */
  const SETTING_MAPPER_NAME = 'element_name';

  /**
   * Name of setting where permanent  value is stored.
   */
  const SETTING_OTHER_VALUE = 'other_value';

  /**
   * Webform element matcher.
   *
   * @var \Drupal\flexmail_webform\CustomFields\WebformElementMatcherInterface
   */
  protected $elementMatcher;

  /**
   * Flex field manager.
   *
   * @var \Drupal\flexmail_webform\CustomFields\FlexCustomFieldPluginManager
   */
  protected $flexFieldManager;

  /**
   * CustomFieldBuilder constructor.
   *
   * @param \Drupal\flexmail_webform\CustomFields\WebformElementMatcherInterface $element_matcher
   *   Webform element matcher.
   * @param \Drupal\flexmail_webform\CustomFields\FlexCustomFieldPluginManager $flex_field_manager
   *   Flex field manager.
   */
  public function __construct(
    WebformElementMatcherInterface $element_matcher,
    FlexCustomFieldPluginManager $flex_field_manager
  ) {
    $this->elementMatcher = $element_matcher;
    $this->flexFieldManager = $flex_field_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function buildCustomMappingFields(WebformHandlerInterface $handler, ?FlexmailApiManagerInterface $flexmailApiManager): array {
    $elements = [];
    if (!$flexmailApiManager) {
      return $elements;
    }

    $elements['custom'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Flexmail fields'),
      '#description' => $this->t('You can map additional fields from your webform to custom fields in your Flexmail account.
      Note that multi value fields must have the same values'),
    ];

    $flex_fields = $flexmailApiManager->getCustomFields();
    foreach ($flex_fields as $flex_field_data) {
      $flex_field_key = $flex_field_data['placeholder'];

      $element = $this->buildElementForFlexField($handler, $flex_field_data);
      if ($element) {
        $elements['custom'][$flex_field_key] = $element;
      }
    }

    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  public function getCustomSettings(WebformHandlerInterface $handler): array {
    $defaults = $handler->getConfiguration();
    if (!isset($defaults['settings']['custom'])) {
      return [];
    }

    return $defaults['settings']['custom'];
  }

  /**
   * Get form element for single flex filed.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $handler
   *   Webform Handler.
   * @param array $flex_field_data
   *   Flex custom field data.
   *
   * @return array
   *   Form element
   */
  protected function buildElementForFlexField(WebformHandlerInterface $handler, array $flex_field_data): array {
    $flex_field_name = $flex_field_data['name'];
    $flex_field_type = $flex_field_data['type'];
    $flex_field_key = $flex_field_data['placeholder'];

    /** @var \Drupal\flexmail_webform\CustomFields\FlexCustomFieldInterface $custom_field */
    try {
      $flex_plugin = $this->flexFieldManager->createInstance($flex_field_type, $flex_field_data);
    }
    catch (PluginException $e) {
      return [];
    }

    $default_settings = $this->getDefaultElementValue($handler, $flex_field_key);

    $element = [];
    $element[self::SETTING_MAPPER_NAME] = [
      '#type' => 'select',
      '#required' => FALSE,
      '#empty_option' => $this->t('- None -'),
      '#title' => $this->t('@name Flexmail field', ['@name' => $flex_field_name]),
      '#options' => $this->generateOptionList($handler->getWebform(), $flex_field_type),
      '#default_value' => $default_settings[self::SETTING_MAPPER_NAME] ?? NULL,
    ];

    $dropdown_selector_path = ':input[name="settings[flexmail][account_fields][custom]' .
      '[' . $flex_field_key . '][' . self::SETTING_MAPPER_NAME . ']"]';
    $element[self::SETTING_OTHER_VALUE] = $flex_plugin->getElementForStaticValue();
    $element[self::SETTING_OTHER_VALUE]['#states'] = [
      'visible' => [
        $dropdown_selector_path => ['value' => self::OTHER_OPTION],
      ],
      'required' => [
        $dropdown_selector_path => ['value' => self::OTHER_OPTION],
      ],
    ];
    $element[self::SETTING_OTHER_VALUE]['#default_value'] = $default_settings[self::SETTING_OTHER_VALUE] ?? NULL;

    return $element;
  }

  /**
   * Generate option list.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   Webform entity.
   * @param string $flex_field_type
   *   Flex field type.
   *
   * @return array
   *   Options.
   */
  protected function generateOptionList(WebformInterface $webform, string $flex_field_type): array {
    return [
      self::OTHER_OPTION => $this->t('Custom value'),
      $this->t('Webform elements')->render() =>
      $this->elementMatcher->getOptionsForFlexType($webform, $flex_field_type),
    ];
  }

  /**
   * Get default value.
   *
   * @param \Drupal\webform\Plugin\WebformHandlerInterface $handler
   *   Webform handler.
   * @param string $element_key
   *   Field name
   *
   * @return array|null
   *   Form field value
   */
  protected function getDefaultElementValue(WebformHandlerInterface $handler, string $element_key): ?array {
    $settings = $this->getCustomSettings($handler);
    if (!isset($settings[$element_key])) {
      return NULL;
    }

    return $settings[$element_key];
  }

}
