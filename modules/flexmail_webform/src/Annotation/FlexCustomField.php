<?php

namespace Drupal\flexmail_webform\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines FlexCustomField annotation object.
 *
 * @see plugin_api
 *
 * @package Drupal\flexmail_webform\Annotation
 *
 * @Annotation
 */
class FlexCustomField extends Plugin {

  /**
   * Description of plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Supported webform elements types.
   *
   * @var array
   */
  public $supported_webform_element_types = [];

}
