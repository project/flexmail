<?php

namespace Drupal\flexmail_webform\StringTranslation;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Trait FlexWebformStringTranslationTrait.
 *
 * @package Drupal\flexmail_webform\StringTranslation
 */
trait FlexWebformStringTranslationTrait {

  use StringTranslationTrait {
    StringTranslationTrait::t as mainT;
  }

  /**
   * Translates a string to the current language or to a given language.
   *
   * @param string $string
   *   String.
   * @param array $args
   *   Arguments.
   * @param array $options
   *   Options.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function t($string, array $args = [], array $options = []) {
    $options += [
      'context' => 'Flexmail webform',
    ];

    return $this->mainT($string, $args, $options);
  }

}
