<?php

namespace Drupal\flexmail_webform\Plugin\WebformHandler;

use Drupal\flexmail\api\exception\FlexmailAPIException;
use Drupal\flexmail\api\service\FlexmailApiManagerInterface;
use Drupal\flexmail\api\value\Contact;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Create contact submission handler plugin.
 *
 * It can also update existing contacts if configured to do so.
 *
 * @WebformHandler(
 *   id = "flexmail_handler",
 *   label = @Translation("Flexmail handler"),
 *   category = @Translation("flexmail"),
 *   description = @Translation("A flexmail handler can create or update contacts in flexmail."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class FlexmailWebformHandler extends FlexmailWebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function getFields(?FlexmailApiManagerInterface $flexmailApiManager): array {
    $fields = parent::getFields($flexmailApiManager);

    // Preferences.
    $preference_options = [];
    if ($flexmailApiManager) {
      $preferences = $flexmailApiManager->getPreferences();
      if ($preferences['total'] !== 0 && isset($preferences['_embedded']['item'])) {
        foreach ($preferences['_embedded']['item'] as $preference) {
          $preference_options[$preference['id']] = "{$preference['title']} - {$preference['id']}";
        }
      }
    }

    $fields['preference'] = [
      '#type' => 'select',
      '#title' => $this->t('Preference'),
      '#description' => $this->t('Select which preference to use within selected Flexmail account.'),
      '#empty_option' => $this->t('- Select preference -'),
      '#options' => $preference_options,
      '#default_value' => $this->configuration['preference'] ?? NULL,
    ];

    // Interests.
    $interest_options = $this->getInterestOptions($flexmailApiManager);
    if ($interest_options) {
      $fields['interest'] = [
        '#type' => 'select',
        '#title' => $this->t('Interest'),
        '#description' => $this->t('Select which interest to use within selected Flexmail account.'),
        '#empty_option' => $this->t('- Select interest -'),
        '#options' => $interest_options,
        '#default_value' => $this->configuration['interest'] ?? NULL,
      ];
    }

    // Email.
    $fields['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email field'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['email'] ?? NULL,
      '#options' => $this->getWebformFieldsByType([
        'email',
        'webform_email_confirm',
      ]),
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select a webform field for email you want to use when creating contact.'),
    ];

    $textfields = $this->getWebformFieldsByType(['textfield']);
    // First name.
    $fields['first_name'] = [
      '#type' => 'select',
      '#title' => $this->t('First name field'),
      '#default_value' => $this->configuration['first_name'] ?? NULL,
      '#options' => $textfields,
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select a webform field you want to use for first name when creating contact.'),
    ];

    // Name.
    $fields['name'] = [
      '#type' => 'select',
      '#title' => $this->t('Name field'),
      '#default_value' => $this->configuration['name'] ?? NULL,
      '#options' => $textfields,
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select a webform field you want to use for name when creating contact.'),
    ];

    try {
      $custom_fields = $this->customFieldBuilder->buildCustomMappingFields($this, $flexmailApiManager);
      $fields = array_merge($fields, $custom_fields);
    }
    catch (FlexmailAPIException $e) {
      $this->messenger()
        ->addError($this->t('There was a problem pulling custom flex fields.'));
    }

    return $fields;
  }

  /**
   * Get interest options.
   *
   * @param \Drupal\flexmail\api\service\FlexmailApiManagerInterface|null $flexmailApiManager
   *   The api manager.
   *
   * @return array
   *   Interest options.
   */
  protected function getInterestOptions(?FlexmailApiManagerInterface $flexmailApiManager): array {
    $interest_options = [];
    if (!$flexmailApiManager) {
      return $interest_options;
    }

    try {
      $interests = $flexmailApiManager->getInterestLabels();
    }
    catch (FlexmailAPIException $e) {
      $interests = [];

      if ($e->getCode() != 403) {
        $this->messenger()->addWarning(
          $this->t('System is not able to get values for Interest - @message.', ['@message' => $e->getMessage()])
        );
      }
    }
    if ($interests['total'] !== 0 && isset($interests['_embedded']['item'])) {
      foreach ($interests['_embedded']['item'] as $interest) {
        $interest_options[$interest['id']] = "{$interest['name']} - {$interest['id']}";
      }
    }

    return $interest_options;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // If submission update, do nothing.
    if ($update) {
      return;
    }

    $customFields = $this->flexCustomValues->getValuesFromSubmission($this, $webform_submission);

    $flexmailManager = $this->handlerUtility->getFlexmailManager($this);

    // Get submission values.
    $submission_fields = $webform_submission->toArray(TRUE)['data'];
    $submission_email = $submission_fields[$this->configuration['email']];

    // Check first if contact with this email exists.
    $contacts = $flexmailManager->getContactWithEmail($submission_email);
    if ((empty($contacts)) || (isset($contacts['total']) && $contacts['total'] === 0)) {
      // Create contact.
      $newContact = new Contact(
        $submission_email,
        $this->languageManager->getCurrentLanguage()->getId(),
        $this->configuration['source'],
        NULL,
        $submission_fields[$this->configuration['first_name']] ?? '',
        $submission_fields[$this->configuration['name']] ?? '',
        $customFields
      );

      try {
        $contact_id = $flexmailManager->createContact($newContact);
      }
      catch (FlexmailAPIException $e) {
        $this->messenger()
          ->addError($this->t('There was a problem subscribing @email to Flexmail.', ['@email' => $submission_email]));
        return;
      }

    }
    else {
      // Check if the custom fields need to be updated.
      $contactArray = isset($contacts['_embedded']['item']) ? \reset($contacts['_embedded']['item']) : false;
      if ($contactArray) {
        $contact_id = $contactArray['id'];
      }

      if ($this->doWeUpdateExistingContacts()) {
        $updateContact = $contactArray;
        // Update custom fields.
        $diffCustomFields = $this->diffMultiDimensionalArray($customFields, $contactArray['custom_fields']);
        if (!empty($diffCustomFields)) {
          $newCustomFields = $diffCustomFields + $contactArray['custom_fields'];
          $updateContact['custom_fields'] = $newCustomFields;
        }
        // Update first name.
        if (isset($submission_fields[$this->configuration['first_name']])) {
          $updateContact['first_name'] = $submission_fields[$this->configuration['first_name']];
        }
        // Update name.
        if (isset($submission_fields[$this->configuration['name']])) {
          $updateContact['name'] = $submission_fields[$this->configuration['name']];
        }
        // Set/update language.
        $updateContact['language'] = $this->languageManager->getCurrentLanguage()
          ->getId();

        // Update only if there are differences.
        if ($this->contactNeedsUpdating($contactArray, $updateContact)) {
          try {
            $flexmailManager->updateContact($contact_id, Contact::createFromAssocArray($updateContact));
          }
          catch (FlexmailAPIException $e) {
            $this->messenger()
              ->addError($this->t('There was a problem updating @email in Flexmail.', [
                '@email' => $submission_email,
              ]));
          }
        }
        else {
          $this->loggerFactory->get('Flexmail')
            ->notice($this->t('@email was not updated in Flexmail since it already contains same data.', ['@email' => $submission_email]));
        }
      }
    }

    // If something was wrong, log error and return.
    if (empty($contact_id)) {
      $this->loggerFactory->get('Flexmail')
        ->error($this->t('Can not create or retrieve user with email: @email', ['@email' => $submission_email]));
      return;
    }

    // Set contact preferences if required.
    if ($preference = $this->configuration['preference']) {
      // First check if contact is already subscribed to selected preference.
      $contact_preferences = $flexmailManager->getContactPreferenceSubscriptions($contact_id)['_embedded']['item'] ?? [];
      if (array_search($preference, array_column($contact_preferences, 'id')) === FALSE) {
        try {
          $flexmailManager->subscribeContactToPreference($contact_id, $preference);
        }
        catch (FlexmailAPIException $e) {
          $this->messenger()
            ->addError($this->t('There was a problem subscribing @email to Flexmail preference @preference.', [
              '@email' => $submission_email,
              '@preference' => $preference,
            ]));
        }
      }
    }

    // Set contact interests if required.
    if ($interest = $this->configuration['interest']) {
      // First check if contact has interest label already assigned.
      $contact_interests = $flexmailManager->getContactInterestLabels($contact_id)['_embedded']['item'] ?? [];
      if (array_search($interest, array_column($contact_interests, 'id')) === FALSE) {
        try {
          $flexmailManager->subscribeContactToInterestLabel($contact_id, $interest);
        }
        catch (FlexmailAPIException $e) {
          $this->messenger()
            ->addError($this->t('There was a problem subscribing @email to Flexmail interest @interest.', [
              '@email' => $submission_email,
              '@interest' => $interest,
            ]));
        }
      }
    }
  }

  /**
   * Helper function which checks if we actually need to update contact.
   *
   * @param array $old_contact_data
   *   The data from the old contact.
   * @param array $new_contact_data
   *   The data from the new contact.
   *
   * @return bool
   *   Whether or not the contact needs an update or not.
   */
  private function contactNeedsUpdating(array $old_contact_data, array $new_contact_data): bool {
    // First check custom fields.
    $diff = $this->diffMultiDimensionalArray(
      $old_contact_data['custom_fields'],
      $new_contact_data['custom_fields']
    );
    if (!empty($diff)) {
      return TRUE;
    }
    // Now check other fields.
    // array_diff produces warning for nested arrays.
    // We checked those first and can be removed now.
    unset($old_contact_data['custom_fields']);
    unset($new_contact_data['custom_fields']);

    return !empty(array_diff($old_contact_data, $new_contact_data));
  }

  /**
   * Diff multi dimensional array.
   *
   * @param array $first
   *   The array to compare from.
   * @param array $second
   *   Arrays to compare against.
   *
   * @return array
   *   Returns an array containing all the entries from first array
   *   that are not present in any of the second arrays.
   */
  protected function diffMultiDimensionalArray(array $first, array $second): array {
    $diff = array_diff(
      array_map('serialize', $first),
      array_map('serialize', $second)
    );

    return array_map('unserialize', $diff);
  }

}
