<?php

namespace Drupal\flexmail_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\flexmail\api\service\FlexmailApiManagerInterface;
use Drupal\flexmail\Entity\FlexmailAccount;
use Drupal\flexmail_webform\StringTranslation\FlexWebformStringTranslationTrait;
use Drupal\webform\Plugin\WebformHandlerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FlexmailWebformHandler.
 */
abstract class FlexmailWebformHandlerBase extends WebformHandlerBase {

  use FlexWebformStringTranslationTrait;

  /**
   * Flexmail factory.
   *
   * @var \Drupal\flexmail\api\service\FlexmailApiFactoryInterface
   */
  protected $flexmailFactory;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Custom field builder.
   *
   * @var \Drupal\flexmail_webform\CustomFields\CustomFieldBuilderInterface
   */
  protected $customFieldBuilder;

  /**
   * Flex custom values service.
   *
   * @var \Drupal\flexmail_webform\CustomFields\FlexCustomValuesInterface
   */
  protected $flexCustomValues;

  /**
   * Handler utility.
   *
   * @var \Drupal\flexmail_webform\WebformHandlerUtility
   *   Handler utility.
   */
  protected $handlerUtility;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->flexmailFactory = $container->get('flexmail.factory');
    $instance->languageManager = $container->get('language_manager');
    $instance->customFieldBuilder = $container->get('flexmail_webform.custom_field_builder');
    $instance->flexCustomValues = $container->get('flexmail_webform.flex_custom_values');
    $instance->handlerUtility = $container->get('flexmail_webform.webform_handler_utility');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $account_summary = '<strong>' . $this->t('Flexmail account') . ': </strong>' . $this->configuration['account'];
    $source_summary = '<strong>' . $this->t('Source') . ': </strong>' . $this->configuration['source'];

    $markup = "$account_summary<br/>$source_summary";
    return [
      '#markup' => $markup,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['flexmail'] = [
      '#type' => 'details',
      '#title' => $this->t('Handler settings'),
      '#open' => TRUE,
      '#attributes' => ['id' => 'flexmail-handler-settings'],
    ];

    $form['flexmail']['account'] = [
      '#type' => 'select',
      '#title' => $this->t('Flexmail account'),
      '#description' => $this->t('Select which Flexmail account to use with this handler.'),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select an account -'),
      '#options' => $this->getFlexmailAccountOptions(),
      '#default_value' => $this->configuration['account'] ?? NULL,
      '#ajax' => [
        'callback' => [$this, 'updateHandlerAjaxCallback'],
        'event' => 'change',
        'wrapper' => 'flexmail-handler-settings',
      ],
    ];

    $flexmailApiManager = $this->getFlexmailApiManager($form, $form_state);

    // Get all the fields.
    $form['flexmail']['account_fields'] = $this->getFields($flexmailApiManager);

    // Add update existing contacts radio buttons.
    $existingContactOptions = [
      'nothing' => $this->t('Do not update contact'),
      'update' => $this->t('Update contact'),
    ];

    $form['flexmail']['existing_contact'] = [
      '#type' => 'radios',
      '#title' => $this->t('Update existing contact?'),
      '#options' => $existingContactOptions,
      '#default_value' => isset($this->configuration['existing_contact']) ? $this->configuration['existing_contact'] : 'nothing',
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Get options list of all available Flexmail accounts.
   *
   * @return array
   *   List with the available Flexmail accounts.
   */
  private function getFlexmailAccountOptions() {
    $options = [];
    $accounts = FlexmailAccount::loadMultiple();

    foreach ($accounts as $account) {
      $options[$account->id()] = $account->label();
    }

    return $options;
  }

  /**
   * Get selected Flexmail account on form and load FlexmailApiManager with it.
   *
   * @param array $form
   *   An associative array containing the initial structure of the plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\flexmail\api\service\FlexmailApiManagerInterface|null
   *   The FlexmailApiManager loaded from the selected account.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFlexmailApiManager(array &$form, FormStateInterface $form_state): ?FlexmailApiManagerInterface {
    // Try to get the selected text from the select element on our form.
    if (!empty($form_state->getUserInput()['settings']['flexmail']['account'])) {
      $account_id = $form_state->getUserInput()['settings']['flexmail']['account'];
    }
    elseif (!empty($this->configuration['account'])) {
      // Backup plan is to load it from the saved configurations.
      $account_id = $this->configuration['account'];
    }

    // We load the account from the id and send it back.
    if (!empty($account_id)) {
      $flexmailAccountStorage = $this->entityTypeManager->getStorage('flexmail_account');
      /** @var \Drupal\flexmail\Entity\FlexmailAccount $account */
      $account = $flexmailAccountStorage->load($account_id);
      return $this->flexmailFactory->get($account);
    }

    return NULL;
  }

  /**
   * Retrieves fields on webform by type.
   *
   * @param array $types
   *   The type of field from the webform.
   *
   * @return array
   *   The fields that match the type.
   */
  protected function getWebformFieldsByType(array $types): array {
    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $options = [];

    foreach ($fields as $field_name => $field) {
      if (in_array($field['#type'], $types)) {
        $options[$field_name] = $field['#title'];
      }
    }

    return $options;
  }

  /**
   * Get all fields for this handler.
   *
   * @param \Drupal\flexmail\api\service\FlexmailApiManagerInterface|null $flexmailApiManager
   *   The api manager.
   *
   * @return array
   *   All the fields available for this handler.
   *
   * @throws \Drupal\flexmail\api\exception\FlexmailAPIException
   */
  public function getFields(?FlexmailApiManagerInterface $flexmailApiManager): array {
    // Load all the sources from the api manager.
    $source_options = [];
    if ($flexmailApiManager) {
      $sources = $flexmailApiManager->getSources();
      if ($sources['total'] !== 0 && isset($sources['_embedded']['item'])) {
        foreach ($sources['_embedded']['item'] as $source) {
          $source_options[$source['id']] = "{$source['name']} - {$source['id']}";
        }
      }
    }

    $fields['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#description' => $this->t('Select which source to use within selected Flexmail account.'),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select source -'),
      '#options' => $source_options,
      '#default_value' => $this->configuration['source'] ?? NULL,
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    // Validate that selected Flexmail account exists.
    $account_id = $form_state->getValue('flexmail')['account'];
    if (!empty($account_id)) {
      $account = $this->entityTypeManager->getStorage('flexmail_account')
        ->load($account_id);
      if (empty($account)) {
        $form_state->setErrorByName('flexmail][account', $this->t('%account_id account does not exist.', ['%account_id' => $account_id]));
      }
    }
  }

  /**
   * Do we update contacts?
   *
   * Helper function which returns if we will update a contact or not.
   * In case it already exists.
   *
   * @return bool
   *   Returns TRUE if we update the contacts.
   */
  protected function doWeUpdateExistingContacts(): bool {
    return isset($this->configuration['existing_contact']) ? $this->configuration['existing_contact'] === 'update' : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // Save account.
    $values = $form_state->getValues()['flexmail'];
    if (isset($values['account'])) {
      $this->configuration['account'] = $values['account'];
    }

    if (isset($values['existing_contact'])) {
      $this->configuration['existing_contact'] = $values['existing_contact'];
    }

    // Save account fields.
    foreach ($values['account_fields'] as $field => $value) {
      $this->configuration[$field] = $value;
    }
  }

  /**
   * Ajax callback to update handler settings.
   *
   * @param array $form
   *   An associative array containing the initial structure of the plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The part of the form that will be updated with ajax.
   */
  public function updateHandlerAjaxCallback(array &$form, FormStateInterface $form_state): array {
    return $form['settings']['flexmail'];
  }

}
