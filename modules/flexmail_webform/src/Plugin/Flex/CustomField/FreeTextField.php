<?php

namespace Drupal\flexmail_webform\Plugin\Flex\CustomField;

use Drupal\flexmail_webform\CustomFields\FlexCustomFieldBase;

/**
 * Resembles the FreeTextField from Flexmail.
 *
 * @FlexCustomField(
 *   id = "free_text",
 *   description = @Translation("Free text flex mapper."),
 *   supported_webform_element_types = {
 *      "textfield", "textarea", "number", "email", "tel", "url", "hidden"
 *   }
 * )
 *
 * @package Drupal\flexmail_webform\Plugin\Flex\FieldMapper
 */
class FreeTextField extends FlexCustomFieldBase {

  /**
   * {@inheritDoc}
   */
  public function formatTypedValue($element_value) {
    return $element_value;
  }

  /**
   * {@inheritDoc}
   */
  public function formatStaticValue($element_value) {
    return $element_value;
  }

  /**
   * {@inheritDoc}
   */
  public function getElementForStaticValue(): array {
    return [
      '#type' => 'textfield',
    ];
  }

}
