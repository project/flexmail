<?php

namespace Drupal\flexmail_webform\Plugin\Flex\CustomField;

use Drupal\flexmail_webform\CustomFields\FlexCustomFieldBase;

/**
 * Resembles the DateField from Flexmail.
 *
 * @FlexCustomField(
 *   id = "date",
 *   description = @Translation("Date mapper."),
 *   supported_webform_element_types = {
 *      "date", "hidden"
 *   }
 * )
 *
 * @package Drupal\flexmail_webform\Plugin\Flex\FieldMapper
 */
class DateField extends FlexCustomFieldBase {

  /**
   * {@inheritDoc}
   */
  public function formatTypedValue($element_value) {
    return $element_value;
  }

  /**
   * {@inheritDoc}
   */
  public function formatStaticValue($element_value) {
    return $element_value;
  }

  /**
   * {@inheritDoc}
   */
  public function getElementForStaticValue(): array {
    return [
      '#type' => 'date',
      '#date_date_format' => 'd/m/Y',
    ];
  }

}
