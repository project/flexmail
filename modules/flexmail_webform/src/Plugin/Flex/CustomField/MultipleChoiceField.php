<?php

namespace Drupal\flexmail_webform\Plugin\Flex\CustomField;

use Drupal\flexmail_webform\CustomFields\FlexCustomFieldBase;
use Drupal\flexmail_webform\StringTranslation\FlexWebformStringTranslationTrait;

/**
 * Resembles the MultipleChoiceField from Flexmail.
 *
 * @FlexCustomField(
 *   id = "multiple_choice",
 *   description = @Translation("Multiple choice mapper."),
 *   supported_webform_element_types = {
 *      "checkboxes", "select"
 *   }
 * )
 *
 * @package Drupal\flexmail_webform\Plugin\Flex\FieldMapper
 */
class MultipleChoiceField extends FlexCustomFieldBase {

  use FlexWebformStringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function formatTypedValue($element_value) {
    if ($this->allowsMultipleValues()) {
      return $this->getMultipleValue($element_value);
    }

    return $this->getSingleValue($element_value);
  }

  /**
   * {@inheritDoc}
   */
  public function formatStaticValue($element_value) {
    $element_value = array_map('trim', explode(',', $element_value));

    return $this->formatTypedValue($element_value);
  }

  /**
   * Get single value.
   *
   * @param $element_value
   *   Element value.
   *
   * @return string
   *   Single value.
   */
  protected function getSingleValue($element_value): string {
    if (is_array($element_value)) {
      return reset($element_value);
    }

    return (string) $element_value;
  }

  /**
   * Get multiple value.
   *
   * @param $element_value
   *   Element value.
   *
   * @return array
   *   Multiple value.
   */
  protected function getMultipleValue($element_value): array {
    if (is_array($element_value)) {
      return $element_value;
    }
    return [$element_value];
  }

  /**
   * {@inheritDoc}
   */
  public function getElementForStaticValue(): array {
    if ($this->allowsMultipleValues()) {
      $description = 'You need to use the same values as in Flexmail system. Use commas to separate values for multiple responses.';
    }
    else {
      $description = 'You need to use the same value as in Flexmail system';
    }

    return [
      '#type' => 'textfield',
      '#description' => $this->t($description),
    ];
  }

}
