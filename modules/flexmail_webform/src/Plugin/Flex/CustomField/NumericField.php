<?php

namespace Drupal\flexmail_webform\Plugin\Flex\CustomField;

use Drupal\flexmail_webform\CustomFields\FlexCustomFieldBase;

/**
 * Resembles the NumericField from Flexmail.
 *
 * @FlexCustomField(
 *   id = "numeric",
 *   description = @Translation("Numeric mapper."),
 *   supported_webform_element_types = {
 *      "number", "hidden"
 *   }
 * )
 *
 * @package Drupal\flexmail_webform\Plugin\Flex\FieldMapper
 */
class NumericField extends FlexCustomFieldBase {

  /**
   * {@inheritDoc}
   */
  public function formatTypedValue($element_value) {
    if (is_numeric($element_value)) {
      return (float) $element_value;
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function formatStaticValue($element_value) {
    return $this->formatTypedValue($element_value);
  }

  /**
   * {@inheritDoc}
   */
  public function getElementForStaticValue(): array {
    return [
      '#type' => 'number',
    ];
  }

}
